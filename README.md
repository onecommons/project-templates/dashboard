# Your dashboard project

Unfurl Cloud commits your dashboard's environments and deployments to the git repository in this project.

You can [clone this project locally](/home#clone-instructions) and keep all your settings and deployment data in sync as well deploy locally using the [unfurl](https://docs.unfurl.run) command-line.
